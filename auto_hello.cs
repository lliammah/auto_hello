using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

/*****************************************************************************************
 * 31/03/2022 - 0.5 - Рефакторинг вывода отладочной информации.
 * 26/03/2022 - 0.4 - Сообщения вынесены в настройки скрипта.
 * 06/02/2022 - 0.3 - Убрана зависимость от библиотеки. Произведён рефакторинг кода. В
 *                    настройки добавлены Боты, Админы и Чёрный список.
 * 15/07/2021 - 0.2 - Подключение общей библиотеки. Не приветствовать админов и ботов.
 * xx/xx/xxxx - 0.1 - Приветствие нового зрителя.
 *****************************************************************************************
 * Как можно отблагодарить:
 * Оформить удобную для вас подписку на Boosty.to - https://boosty.to/lliammah
 * Разово поддержать через DonationAlerts - https://www.donationalerts.com/r/lliammah
*****************************************************************************************/


namespace RutonyChat
{
    public class AutoHello
    {

        public ScriptInfo si = new ScriptInfo();
        public DebugMode debug = new DebugMode();
        public Config config = new Config();

        public const int MSG_DANGER = 1;
        public const int MSG_WARNING = 2;
        public const int MSG_INFO = 3;

        public List<string> ListHello = new List<string>();

        public string msg_number = "";
        public char pad = '0';
        public string str_temp = "";

        public Random rnd = new Random();
        public int choise_message = -1;

        public async void InitParams(Dictionary<string, string> param)
        {
            // Включен или нет режим отладки
            if (param.ContainsKey("DebugMode"))
            {
                try
                {
                    debug.Mode = bool.Parse(param["DebugMode"]);
                }
                catch { }
            }

            // Уровень отладки
            if (param.ContainsKey("DebugLevel"))
            {
                try
                {
                    var sTmp = param["DebugLevel"];
                    if (!string.IsNullOrEmpty(sTmp)) debug.Level = int.Parse(sTmp[0].ToString());
                }
                catch { }
            }

            // БОТЫ
            // Список ников ботов
            if (param.ContainsKey("BotList"))
            {
                try
                {
                    config.BotsList = StringSplit(param["BotList"]);
                }
                catch { }
            }

            // АДМИНЫ
            // Приветствовать админов канала
            if (param.ContainsKey("WelcomeAdmin"))
            {
                try { config.WelcomeAdmin = bool.Parse(param["WelcomeAdmin"]); }
                catch { }
            }
            // Список ников админов
            if (param.ContainsKey("AdminList"))
            {
                try
                {
                    config.AdminList = StringSplit(param["AdminList"]);
                }
                catch { }
            }

            // ЧЁРНЫЙ СПИСОК
            // Список ников сообщения от кого не нужно дублировать
            if (param.ContainsKey("BlackNiks"))
            {
                try
                {
                    config.BlackList = StringSplit(param["BlackNiks"]);
                }
                catch { }
            }

            // Загрузим сообщения
            for (int i = 1; i <= 20; i++)
            {
                msg_number = i.ToString().PadLeft(2, pad);
                msg_number = "WelcomeMessage_" + msg_number;
                if (param.ContainsKey(msg_number))
                {
                    // Write_Log(MSG_INFO, "msg_number", msg_number);
                    try
                    {
                        str_temp = param[msg_number];
                        // Write_Log(MSG_INFO, "str_temp", str_temp);
                        if (!string.IsNullOrEmpty(str_temp))
                        {
                            ListHello.Add(str_temp);
                            // Write_Log(MSG_INFO, "Добавили строку " + msg_number + ": " + str_temp);
                        }
                    }
                    catch { }
                }
            }
            if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Строк с приветствием", ListHello.Count.ToString());

            RutonyBot.SayToWindow(si.Name, string.Format(si.Info, si.Name, si.Version, ""));
        }

        public void Closing()
        {
            RutonyBot.SayToWindow(si.Name, string.Format(si.Info, si.Name, si.Version, "dis"));
        }

        // Заглушка для стандартного метода
        public void NewMessage(string site, string name, string text, bool system) { }

        public void NewAlert(string site, string typeEvent, string subplan, string name, string text, float donate, string currency, int qty)
        {
            if (typeEvent == "new_viewer")
            {
                if (IsName(config.BotsList, name, "Проверяем ботов"))
                {
                    if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Bot", name);
                    return;
                }

                if (!config.WelcomeAdmin)
                {
                    if (IsName(config.AdminList, name, "Проверяем админов"))
                    {
                        if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Admin", name);
                        return;
                    }
                }

                if (IsName(config.BlackList, name, "Проверяем чёрный список"))
                {
                    if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Black Nick", name);
                    return;
                }

                // if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "LastMessageID", config.LastMessageID.ToString());
                while (true)
                {
                    // Write_Log(MSG_INFO, "ListHello.Count", (ListHello.Count - 1).ToString());
                    choise_message = GetRnd(ListHello.Count - 1);
                    // Write_Log(MSG_INFO, "choise_message", choise_message.ToString());
                    if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "choise_message: " + choise_message.ToString() + " / LastMessageID: " + config.LastMessageID.ToString());
                    if (choise_message != config.LastMessageID)
                    {
                        config.LastMessageID = choise_message;
                        break;
                    }
                }



                string msg = ListHello[choise_message].Replace("$name", name);
                if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "site: " + site + " / msg: " + msg);

                switch (site.ToLower())
                {
                    case "twitch":
                        RutonyBot.TwitchBot.Say(msg);
                        break;
                    case "goodgame":
                        RutonyBot.GoodgameBot.Say(msg);
                        break;
                    case "youtube":
                        RutonyBot.YoutubeBot.Say(msg);
                        break;
                    default:
                        RutonyBot.BotSay(site, msg);
                        break;
                }
            }
        }




        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Есть ли ник в нужном нам списке
        public bool IsName(string[] names, string name, string desc = "")
        {
            string fnc_title = "Поиск имени в массиве";
            var n = name.Trim().ToLower();
            if (debug.Mode && debug.Level >= 2)
            {
                Write_Log(MSG_INFO, "Что проверяем: <span style='color:#FFFF00;'>" + desc + "</span> / Размер массива: <span style='color:#FFFF00;'>" + names.Length.ToString() + "</span>");
                Write_Log(MSG_INFO, fnc_title, name);
            }
            if (names.Length == 0)
            {
                return false;
            }
            else
            {
                return names.Any(Nik => string.Equals(n, Nik.Trim().ToLower(), StringComparison.CurrentCultureIgnoreCase));
            }
        }

        // Поделим строку
        public string[] StringSplit(string str)
        {
            char[] separators = new char[] { ',' };
            return str.Trim().Replace(" ", "").Split(separators, StringSplitOptions.RemoveEmptyEntries);
        }



        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Генерируем случайное число
        private int GetRnd(int max, int min = 0)
        {
            if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "min: " + min.ToString() + " / max: " + max.ToString());
            return rnd.Next(min, max);
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Выведим строку с отладочной информацией
        public void Write_Log(int msg_type, string str1, string str2 = "")
        {
            string new_string = "";
            switch (msg_type)
            {
                case MSG_DANGER:
                    if (str2 == "") new_string = str1;
                    else new_string = string.Format(debug.MsgDanger, str1, str2);
                    break;
                case MSG_WARNING:
                    if (str2 == "") new_string = str1;
                    else new_string = string.Format(debug.MsgWarning, str1, str2);
                    break;
                case MSG_INFO:
                    if (str2 == "") new_string = str1;
                    else new_string = string.Format(debug.MsgInfo, str1, str2);
                    break;
                default:
                    if (str2 == "") new_string = str1;
                    else new_string = str1 + " " + str2;
                    break;
            }
            RutonyBot.SayToWindow(si.Name, new_string);
        }

    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Объединение параметров для удобства
    public class Config
    {
        public string[] BotsList = { };
        public bool WelcomeAdmin = true;
        public string[] AdminList = { };
        public string[] BlackList = { };
        public int LastMessageID = -1;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Режим и уровень отлпдки для вывода информационных сообщений.
    public class DebugMode
    {
        public bool Mode = false;
        public int Level = 0;

        // Разное
        public string MsgDanger = "{0}: <span style='color:#FF0000;'>{1}</span>";
        public string MsgWarning = "{0}: <span style='color:#FFFF00;'>{1}</span>";
        public string MsgInfo = "{0}: <span style='color:#00FF00;'>{1}</span>";
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Информация по скрипту. Для удобства вынесена в отдельный класс.
    public class ScriptInfo
    {
        public string Info { get; private set; }
        public string Name { get; private set; }
        public string Version { get; private set; }

        public ScriptInfo()
        {
            this.Info = "<span style='color:yellow;'>{0}</span> script, version <span style='color:#00FF00;'>{1}</span> {2}connected";
            this.Name = "Auto Hello";
            this.Version = "0.5";
        }

    }
}